var image;
Template.editimage.helpers({
  editingImage: function () {
    image = Collections.Images.findOne({_id:this.toString()}); 
    return image;
  }
 
});

Template.editimage.events = {
  'click #rotate': function(event){
      $('#edit-image').attr('src',$('#edit-image').attr('src').substring(0, $('#edit-image').attr('src').indexOf('&store'))+'&store=rotate');
  },
  'click #translate': function(event){
      $('#edit-image').attr('src',$('#edit-image').attr('src').substring(0, $('#edit-image').attr('src').indexOf('&store'))+'&store=translate');
  },
  'click #scale': function(event){
      $('#edit-image').attr('src',$('#edit-image').attr('src').substring(0, $('#edit-image').attr('src').indexOf('&store'))+'&store=scale');
  },
  'click #opacity': function(event){
      $('#edit-image').attr('src',$('#edit-image').attr('src').substring(0, $('#edit-image').attr('src').indexOf('&store'))+'&store=opacity');
  },
  'click #reset': function(event){
      $('#edit-image').attr('src',$('#edit-image').attr('src').substring(0, $('#edit-image').attr('src').indexOf('&store'))+'&store=images');
  }
}    