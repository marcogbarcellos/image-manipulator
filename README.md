# Image Manipulation

This project was built with the Meteor platform!

#GraphicMagick

It's necessary to have GraphicMagick(an image manipulation open source tool) installed on your machine to run the project, to download it, go to the following site:
http://www.graphicsmagick.org/
Installing for ubuntu could be done like this:
$ sudo add-apt-repository ppa:dhor/myway
$ sudo apt-get update
$ sudo apt-get install graphicsmagick

To run it follow the next steps:
1. Unzip or download the code
2. cd /your_path/
3. meteor

